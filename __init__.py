import gc, os
from datetime import timedelta, datetime
from functools import wraps

from MySQLdb import escape_string as thwart
from flask import Flask, render_template, flash, request, url_for, redirect, session, make_response, send_file, send_from_directory, jsonify
from passlib.hash import sha256_crypt
from wtforms import Form, TextField, PasswordField, BooleanField, validators
import pygal
from dbconnect import connection
from content_management import Content
from flask_mail import Mail, Message

## ForPayPal integration
from werkzeug.datastructures import ImmutableOrderedMultiDict
import requests


TOPIC_DICT = Content()

app = Flask(__name__, instance_path='/Users/anujkatiyal/Developer/PyCharm/flasktuts/static/protected/')

app.config.update(
    DEBUG=True,
    #EMAIL SETTINGS
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True,
    MAIL_USERNAME='your@gmail.com',
    MAIL_PASSWORD='yourpassword'
)

mail = Mail(app)

class User:
    def username(self):
        try:
            return str(session['username'])
        except:
            return("guest")

user = User()


@app.route('/<path:urlpath>', methods=['GET', 'POST'])  ## can take care of all urls expect those explicitly defined.
@app.route('/', methods=['GET', 'POST'])
def main(urlpath='/'):
    return render_template("main.html")


@app.route('/send-mail/')
def send_mail():
    try:
        msg = Message("Send Mail Tutorial!",
                      sender="yoursendingemail@gmail.com",
                      recipients=["anujk3@icloud.com"])
        msg.body = "Yo!\nHave you heard the good word of Python???"
        mail.send(msg)
        return 'Mail sent!'
    except Exception as e:
        return(str(e))


def special_requirement(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        try:
            if 'testy' == session['username']:
                print("YES")
                return f(*args, **kwargs)
            else:
                return redirect(url_for('dashboard'))
        except Exception as e:
            return redirect(url_for('dashboard'))
    return wrap


@app.route('/protected/<path:filename>')
@special_requirement
def protected(filename):
    try:
        print(app.instance_path)
        return send_from_directory(
            os.path.join(app.instance_path, ''),
            filename
        )
    except Exception as e:
        print(str(e))
        return redirect(url_for('main'))


@app.route('/file-downloads/')
def file_downloads():
    try:
        return render_template('downloads.html')
    except Exception as e:
        return str(e)


@app.route('/return-files/')
def return_files_tut():
    try:
        return send_file('static/files/check.pdf', attachment_filename='check.pdf')
    except Exception as e:
        return str(e)


@app.route('/interactive/')
def interactive():
    return render_template('interactive.html')


@app.route('/background_process')
def background_process():
    try:
        lang = request.args.get('proglang', 0, type=str)
        if lang.lower() == 'python':
            return jsonify(result='You are wise')
        else:
            return jsonify(result='Try again.')
    except Exception as e:
        return str(e)


@app.route('/pygalexample/')
def pygalexample():
    try:
        graph = pygal.Line()
        graph.title = '% Change Coolness of programming languages over time.'
        graph.x_labels = ['2011','2012','2013','2014','2015','2016']
        graph.add('Python',  [15, 31, 89, 200, 356, 900])
        graph.add('Java',    [15, 45, 76, 80,  91,  95])
        graph.add('C++',     [5,  51, 54, 102, 150, 201])
        graph.add('All others combined!',  [5, 15, 21, 55, 92, 105])
        graph_data = graph.render_data_uri()
        return render_template("graphing.html", graph_data = graph_data)
    except Exception as e:
        return(str(e))


@app.route('/include_example/')
def include_example():
    replies = {'Jack':'Cool post',
               'Jane':'+1',
               'Erika':'Most definitely',
               'Bob':'wow',
               'Carl':'amazing!',}
    return render_template("includes_tutorial.html", replies = replies)


@app.route('/purchase/')
def purchase():
    try:
        return render_template("subscribe.html")
    except Exception as e:
        return(str(e))


@app.route('/success/')
def success():
    try:
        return render_template("success.html")
    except Exception as e:
        return(str(e))


@app.route('/ipn/',methods=['POST'])
def ipn():
    try:
        arg = ''
        request.parameter_storage_class = ImmutableOrderedMultiDict
        values = request.form
        for x, y in values.iteritems():
            arg += "&{x}={y}".format(x=x,y=y)

        validate_url = 'https://www.sandbox.paypal.com' \
                       '/cgi-bin/webscr?cmd=_notify-validate{arg}' \
            .format(arg=arg)
        r = requests.get(validate_url)
        if r.text == 'VERIFIED':
            try:
                payer_email =  thwart(request.form.get('payer_email'))
                unix = int(time.time())
                payment_date = thwart(request.form.get('payment_date'))
                username = thwart(request.form.get('custom'))
                last_name = thwart(request.form.get('last_name'))
                payment_gross = thwart(request.form.get('payment_gross'))
                payment_fee = thwart(request.form.get('payment_fee'))
                payment_net = float(payment_gross) - float(payment_fee)
                payment_status = thwart(request.form.get('payment_status'))
                txn_id = thwart(request.form.get('txn_id'))
            except Exception as e:
                with open('/tmp/ipnout.txt','a') as f:
                    data = 'ERROR WITH IPN DATA\n'+str(values)+'\n'
                    f.write(data)

            with open('/tmp/ipnout.txt','a') as f:
                data = 'SUCCESS\n'+str(values)+'\n'
                f.write(data)

            c,conn = connection()
            c.execute("INSERT INTO ipn (unix, payment_date, username, last_name, payment_gross, payment_fee, payment_net, payment_status, txn_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                      (unix, payment_date, username, last_name, payment_gross, payment_fee, payment_net, payment_status, txn_id))
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

        else:
            with open('/tmp/ipnout.txt','a') as f:
                data = 'FAILURE\n'+str(values)+'\n'
                f.write(data)

        return r.text
    except Exception as e:
        return str(e)


# @app.route('/converters/')
# @app.route('/converters/<page>/')
# def convertersexample(page=1):
#     try:
#         return render_template("converterexample.html", page=page)
#     except Exception as e:
#         return(str(e))


# @app.route('/converters/')
# @app.route('/converters/<int:page>/')
# def convertersexample(page=1):
#     try:
#         return render_template("converterexample.html", page=page)
#     except Exception as e:
#         return(str(e))

# @app.route('/converters/')
# @app.route('/converters/<float:page>/')
# def convertersexample(page=1):
#     try:
#         return render_template("converterexample.html", page=page)
#     except Exception as e:
#         return(str(e))

# @app.route('/converters/')
# @app.route('/converters/<string:page>/')
# def convertersexample(page=1):
#     try:
#         return render_template("converterexample.html", page=page)
#     except Exception as e:
#         return(str(e))

# @app.route('/converters/')
# @app.route('/converters/<string:article>/<int:page>')
# def convertersexample(article, page=1):
#     try:
#         return render_template("converterexample.html", page=page, article=article)
#     except Exception as e:
#         return(str(e))


@app.route('/converters/')
@app.route('/converters/<path:urlpath>')
def convertersexample(urlpath):
    try:
        return render_template("converterexample.html", urlpath=urlpath)
    except Exception as e:
        return(str(e))


@app.route('/jinjaman/')
def jinjaman():
    try:
        data = [15, '15', 'Python is good','Python, Java, php, SQL, C++','<p><strong>Hey there!</strong></p>']
        return render_template("jinja-templating.html", data=data)
    except Exception as e:
        return(str(e))


def update_user_tracking():
    try:
        completed = str(request.args['completed'])
        if completed in str(TOPIC_DICT.values()):
            client_name, settings, tracking, rank = userinformation()
            if tracking == None:
                tracking = completed
            else:
                if completed not in tracking:
                    tracking = tracking+","+completed

            c,conn = connection()
            c.execute("UPDATE users SET tracking = %s WHERE username = %s",
                      (thwart(tracking),thwart(client_name)))
            conn.commit()
            c.close()
            conn.close()
            client_name, settings, tracking, rank = userinformation()
        else:
            pass


    except Exception as e:
        pass
        #flash(str(e))


def userinformation():
    try:
        client_name = (session['username'])
        guest = False
    except:
        guest = True
        client_name = "Guest"

    if not guest:
        try:
            c,conn = connection()
            c.execute("SELECT * FROM users WHERE username = (%s)", (thwart(client_name),))
            data = c.fetchone()
            settings = data[4]
            tracking = data[5]
            rank = data[6]
        except Exception as e:
            pass

    else:
        settings = [0,0]
        tracking = [0,0]
        rank = [0,0]

    print(settings)

    return client_name, settings, tracking, rank





def topic_completion_percent():
    try:
        client_name, settings, tracking, rank = userinformation()

        try:
            tracking = tracking.split(",")
        except:
            pass

        if tracking == None:
            tracking = []
            #flash("tracking is none")

        completed_percentages = {}

        for each_topic in TOPIC_DICT:
            total = 0
            total_complete = 0

            for each in TOPIC_DICT[each_topic]:
                total += 1
                for done in tracking:
                    if done == each[1]:
                        total_complete += 1

            percent_complete = int(((total_complete*100)/total))
            completed_percentages[each_topic] = percent_complete


        return completed_percentages
    except:
        for each_topic in TOPIC_DICT:
            total = 0
            total_complete = 0

            completed_percentages[each_topic] = 0.0

        return completed_percentages

    pass
    #return basics,pygame,pyopengl,kivy,flask,django,mysql,sqlite,datamanip,dataviz,nltk,svm,clustering,imagerec,forexalgo,robotics,supercomp,tkinter


@app.route('/guided-tutorials/', methods=['GET', 'POST'])
@app.route('/topics/', methods=['GET', 'POST'])
@app.route('/begin/', methods=['GET', 'POST'])
@app.route('/python-programming-tutorials/', methods=['GET', 'POST'])
@app.route('/dashboard/', methods=['GET', 'POST'])
#@login_required
def dashboard():
    #print(TOPIC_DICT)
    try:
        try:
            print("lol")
            client_name, settings, tracking, rank = userinformation()
            print(settings)

            if len(tracking) < 10:
                tracking = "/introduction-to-python-programming/"
            gc.collect()

            if client_name == "Guest":
                flash("Welcome Guest, feel free to browse content. Progress tracking is only available for Logged-in users.")
                tracking = ['None']

            update_user_tracking()

            completed_percentages = topic_completion_percent()

            return render_template("dashboard.html", topics=TOPIC_DICT, tracking=tracking, completed_percentages=completed_percentages)
        except Exception as e:
            return((str(e), "please report errors to hskinsley@gmail.com"))
    except Exception as e:
        return((str(e), "please report errors to hskinsley@gmail.com"))


@app.errorhandler(404)
def page_not_found(e):
    try:
        gc.collect()
        rule = request.path
        if "feed" in rule or "favicon" in rule or "wp-content" in rule or "wp-login" in rule or "wp-login" in rule or "wp-admin" in rule or "xmlrpc" in rule or "tag" in rule or "wp-include" in rule or "style" in rule or "apple-touch" in rule or "genericons" in rule or "topics" in rule or "category" in rule or "index" in rule or "include" in rule or "trackback" in rule or "download" in rule or "viewtopic" in rule or "browserconfig" in rule:
            pass
        else:
            pass
            #errorLogging = open("error404.txt", "a")
            #errorLogging.write(str(rule)+"\n") ## You can log anything that is creating an issue
        flash(str(rule))
        return render_template('404.html'), 404
    except Exception as e:
        return(str(e))


@app.errorhandler(405)
def page_not_found(e):
    return render_template("405.html")


@app.route('/login/', methods=["GET", "POST"])
def login_page():
    error = ''
    try:
        c, conn = connection()
        if request.method == "POST":
            #print("Hello")

            data = c.execute("SELECT * FROM users WHERE username = (%s)", (thwart(request.form['username']),)) # comma in the end

            data = c.fetchone()[2]
            #print(data)

            if sha256_crypt.verify(request.form['password'], data):
                session['logged_in'] = True
                session['username'] = request.form['username']

                flash("You are now logged in")
                return redirect(url_for("dashboard"))

            else:
                error = "Invalid credentials, try again."

        gc.collect()

        return render_template("login.html", error=error)

    except Exception as e:
        #flash(e)
        error = "Invalid credentials, try again. HERE"
        return render_template("login.html", error=error)


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('login_page'))

    return wrap


@app.route("/logout/")
@login_required
def logout():
    session.clear()
    flash("You have been logged out!")
    gc.collect()
    return redirect(url_for('dashboard'))


@app.route('/register/', methods=["GET", "POST"])
def register_page():
    try:
        form = RegistrationForm(request.form)

        if request.method == "POST" and form.validate():
            username = form.username.data
            email = form.email.data
            password = sha256_crypt.encrypt((str(form.password.data)))
            c, conn = connection()

            x = c.execute("SELECT * FROM users WHERE username = (%s)", (thwart(username),)) # take care of the comma in end

            if int(x) > 0:
                flash("That username is already taken, please choose another")
                return render_template('register.html', form=form)

            else:
                c.execute("INSERT INTO users (username, password, email, tracking) VALUES (%s, %s, %s, %s)",
                          (thwart(username), thwart(password), thwart(email), thwart("/introduction-to-python-programming/")))

                conn.commit()
                flash("Thanks for registering!")
                c.close()
                conn.close()
                gc.collect()

                session['logged_in'] = True
                session['username'] = username

                return redirect(url_for('dashboard'))

        return render_template("register.html", form=form)

    except Exception as e:
        return(str(e))

@app.route('/sitemap.xml', methods=['GET'])
def sitemap():
    try:
        """Generate sitemap.xml. Makes a list of urls and date modified."""
        pages=[]
        ten_days_ago=(datetime.now() - timedelta(days=7)).date().isoformat()
        # static pages
        for rule in app.url_map.iter_rules():
            if "GET" in rule.methods and len(rule.arguments)==0:
                pages.append(
                    ["http://pythonprogramming.net"+str(rule.rule),ten_days_ago]
                )

        sitemap_xml = render_template('sitemap_template.xml', pages=pages)
        #print(sitemap_xml)
        response = make_response(sitemap_xml)
        response.headers["Content-Type"] = "application/xml"
        print(response)
        return response
    except Exception as e:
        return("Error is ", str(e))


class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=20)])
    email = TextField('Email Address', [validators.Length(min=6, max=50)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the Terms of Service and Privacy Notice (updated Jan 22, 2015)', [validators.Required()])



if __name__ == "__main__":
    app.secret_key = 'super secret key'
    app.run()
